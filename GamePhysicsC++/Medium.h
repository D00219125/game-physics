#pragma once
#include "Vec.h"

class Medium 
{
	float density ;
	Vec windVel, DragCoef, gravity;
public:
	Medium(float dens, Vec grav, Vec windVel);
	void setDragVoef(Vec v);
};